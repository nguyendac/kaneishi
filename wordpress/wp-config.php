<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kaneishi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.xYdX}YO.(ss8CrVWylngN!Xi`B0G=pCh$Md;FLDY/&%}Zh+m7vi(@TZW_RWZhSH');
define('SECURE_AUTH_KEY',  '&&,`XUBBZ@wUrH{92}V[|+rO}K#cEyT<tGs,I2rAHdFOLJ)TI6~yk!~`%<J]X8>d');
define('LOGGED_IN_KEY',    '^Z$)zwvPhsdpC4K|hsEy__uMv8+_<CtR<deI!V|%f(*Jg$.[=PlMw7xTa}D0g#R!');
define('NONCE_KEY',        '4EB61vCl8A<eYSGCwn[hS^uv_ZIA@Re64Q4&7)won}4<kMr.1Yu:hvzGdK U[S ,');
define('AUTH_SALT',        't0MH_kV}gm$2jYC<7u3}pqp`e.~!@>|]z+iG>;8~Yf`F$4*_>5k{82p~CR,-`x.~');
define('SECURE_AUTH_SALT', 'k@OH6*x+U~)%$ND]C-*f>Lk2y9X-!-esyV+sF&39_;-e`g@}[us*eLVFg :C`YvT');
define('LOGGED_IN_SALT',   '%v]z;Kn7 8yKwRE@>axa{(y;<|h~NX(w;[q}b86#H_>sN${P)$=N0W93>{!SlKE.');
define('NONCE_SALT',       '&Kx!Ci 0frlJb[(Q7~?(Ls7hdgxoAfv9yQ;aRj8mn*NNi 1HH`)GLX2Q|%wI`~wv');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
