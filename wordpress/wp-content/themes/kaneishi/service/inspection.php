<?php
/*
  Template Name: Inspection Page
 */
get_header();
?>

<body>

<div id="container">
  <header id="header" class="header">
    <?php get_template_part('templates/templates','header')?>
  </header>
  <main>
    <section class="service service4">
      <div class="ttl">
        <div class="row">
          <h2 class="ttl_inner">鈑金・塗装・ドレスアップサービス</h2>
        </div>
      </div>
      <div class="service_inner">
        <div class="row">
          <div class="service_inner_list">
            <dl class="service_box service_car1">
              <dt>
                <h3>
                  <img src="<?php bloginfo('template_url')?>/service/images/tt_car1.png" alt="title">
                  <span>国家資格整備士の優れた技術</span>
                </h3>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/images/img_car1.png" alt="image">
                </figure>
              </dt>
              <dd>
                <p>当社の優れた技術陣が、車の修理、板金、塗装、車検・整備を承ります。リビルトパーツ、中古パーツも取り扱い出来、お客様のご予算に応じて最適な方法で整備します。整備後は施工前の状態、今の状態を説明し、必要とあれば取り替えた部品の照合なども行っていただけます。見えないところだからこそ、見える形を目指し、お客様の信用と安心に繋げていくサービスを常に考え、ご要望とあれば可能な限りお応えさせていただきます。</p>
              </dd>
            </dl>
            <dl class="service_box service_repair">
              <dt>
                <h3>
                  <img src="<?php bloginfo('template_url')?>/service/images/tt_repair.png" alt="title">
                  <span>小キズから大きな鈑金修理まで全ておまかせ！</span>
                </h3>
              </dt>
              <dd>
                <p>キズやヘコミを放置していませんか？<br>塗装が剥がれたボディは、放っておくとサビが侵食しボディ全体に広がる恐れがあります。弊社では、丁寧でスピーディーな技術をもって鈑金塗装に力を入れています。<br>お客様のご予算に合わせたお見積もりを行いますので、是非ご相談ください。お客様の大切な車のキヅ・ヘコミ・ワレ・交換など、熟練の技で納得の仕上がり。その他に、エアロの販売・取付・全塗装までお受けします。専用の塗装室完備している為、高品質な仕上がりで満足して頂ける価格をご提示致します。</p>
                <ul class="service_repair_gallery">
                  <li>
                    <figure>
                      <figcaption>凹み</figcaption>
                      <img src="<?php bloginfo('template_url')?>/service/images/img_repair1.jpg" alt="image">
                    </figure>
                  </li>
                  <li>
                    <figure>
                      <figcaption>ワレ</figcaption>
                      <img src="<?php bloginfo('template_url')?>/service/images/img_repair2.jpg" alt="image">
                    </figure>
                  </li>
                  <li>
                    <figure>
                      <figcaption>凹み・ワレ</figcaption>
                      <img src="<?php bloginfo('template_url')?>/service/images/img_repair3.jpg" alt="image">
                    </figure>
                  </li>
                  <li>
                    <figure>
                      <figcaption>凹み・キズ</figcaption>
                      <img src="<?php bloginfo('template_url')?>/service/images/img_repair4.jpg" alt="image">
                    </figure>
                  </li>
                </ul>
                <div class="service_repair_list">
                  <dl>
                    <dt>1年保証</dt>
                    <dd>当店では修理させて頂いた箇所には品質の一年保証を付けておりますので、安心して愛車をお乗りいただけます。万が一、修理箇所の異常が出ましたら、無償にて修理いたします。（一部規定有り）</dd>
                  </dl>
                  <dl>
                    <dt>納得の安心価格</dt>
                    <dd>事前にお見積りをお出し、お客様にご納得頂いた上で作業をいたします。当店は車に関する全てのカテゴリを自社で行いますので、他店では到底出せない納得の価格をご提示させて頂きます。</dd>
                  </dl>
                  <dl>
                    <dt>安心の代車無料</dt>
                    <dd>愛車を当店にお預けして頂いてる間、お車が必要な場合もご安心下さい。無料でお使い頂ける代車をご利用ください。禁煙車の指定も可能です。※燃料代は別途必要となります。</dd>
                  </dl>
                  <dl>
                    <dt>出張サービス</dt>
                    <dd>愛車を持て来てただなくても、お電話いただければ、お伺いさせて頂きます。事故車の見積り、車の無料査定も可能ですのでご気軽にお電話ください。</dd>
                  </dl>
                  <dl>
                    <dt>国内全ての<br>保険会社に対応</dt>
                    <dd>保険をお使い頂いて修理される場合もご安心ください。国内全ての保険会社に対応してますので、スピーディーな対応が可能です。保険事故案件もお気軽にお問い合わせください。</dd>
                  </dl>
                  <dl>
                    <dt>ローンでの<br>支払いもOK</dt>
                    <dd>全てローンでの支払いに対応しております。クレジットカード払いも可能です。お気軽にご相談ください。提携会社：オリコ、東芝ファイナンス、全日信販、セディナ、SBI、ユメカード</dd>
                  </dl>
                </div>
              </dd>
            </dl>
            <dl class="service_box service_car1">
              <dt>
                <h3>
                  <img src="<?php bloginfo('template_url')?>/service/images/tt_car1.png" alt="title">
                  <span>国家資格整備士の優れた技術</span>
                </h3>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/images/img_car1.png" alt="image">
                </figure>
              </dt>
              <dd>
                <p>当社の優れた技術陣が、車の修理、板金、塗装、車検・整備を承ります。リビルトパーツ、中古パーツも取り扱い出来、お客様のご予算に応じて最適な方法で整備します。整備後は施工前の状態、今の状態を説明し、必要とあれば取り替えた部品の照合なども行っていただけます。見えないところだからこそ、見える形を目指し、お客様の信用と安心に繋げていくサービスを常に考え、ご要望とあれば可能な限りお応えさせていただきます。</p>
              </dd>
            </dl>
            <dl class="service_box service_car2">
              <dt>
                <h3>
                  <img src="<?php bloginfo('template_url')?>/service/images/tt_car2.png" alt="title">
                  <span>トラブル時に敏速で対応！</span>
                </h3>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/images/img_car2.png" alt="image">
                </figure>
              </dt>
              <dd>
                <p>自社レッカーでどこでも引取り、納車いたします。代車も無料で貸出いたします。事故対応で素早い行動が必要が絶対条件です。弊社は事故などで保険会社に全て任せるのではなく、オーナー様の即座の要望を重視し対応して行きます。</p>
              </dd>
            </dl>
            <dl class="service_box service_car3">
              <dt>
                <h3>
                  <img src="<?php bloginfo('template_url')?>/service/images/tt_car3.png" alt="title">
                  <span>新技術クォーツガラスコーティング</span>
                </h3>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/service/images/img_car3.png" alt="image">
                </figure>
              </dt>
              <dd>
                <p>クォーツガラスコーティングは2000年に商品化され、今日までハイブランドとして多くの方に指示きました。完全無機溶剤のガラス被膜をスプレー技法により吹き付けることにより従来のガラスコーティングに比べ15倍の固さを誇ります。超親水性被膜でイオンデポジットやウォータースポットが付着しづらく、屋外駐車や濃色系のお車に最適なガラスコーティングとなります。</p>
                <p>持久力が高いクォーツガラスコーティングは一般的なガラスコーティングに比べ酸性雨よる劣化や紫外線から塗装面を強固に保護します。耐擦り性能も一般的なコーティングに比べるとその差は歴然です。</p>
              </dd>
            </dl>
          </div>
        </div>
      </div>
    </section>
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/templates','footer')?>
  </footer>
</div>
<?php get_footer();?>
