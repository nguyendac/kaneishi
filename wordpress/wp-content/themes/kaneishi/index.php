<?php get_header();?>
<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/templates','header')?>
    </header>
    <main>
      <section id="fv" class="fv">
        <div class="fv_under" id="under">
          <ul>
            <li><img src="<?php bloginfo('template_url')?>/images/slide01@2x.jpg" alt="slide 01"></li>
            <li><img src="<?php bloginfo('template_url')?>/images/slide02@2x.jpg" alt="slide 02"></li>
            <li><img src="<?php bloginfo('template_url')?>/images/slide03@2x.jpg" alt="slide 03"></li>
            <li><img src="<?php bloginfo('template_url')?>/images/slide04@2x.jpg" alt="slide 04"></li>
          </ul>
        </div>
        <div class="fv_front">
          <h2><img src="<?php bloginfo('template_url')?>/images/intro.png" alt="確かな実績と技術でお応えします！車のことなら、広島県呉市の自動車整備工場「カネイシ自動車」へ！"></h2>
        </div>
      </section><!-- end fv -->
      <section id="news" class="news">
        <div class="news_wrap row">
          <div class="news_ttl">
            <h2>NEWS</h2>
            <p>お知らせ</p>
          </div>
          <ul class="news_list">
            <li><a href="#">
                <time datetime="2018-07-01">2018.07.01</time>
                <span>夏季休業のお知らせ</span>
              </a></li>
            <li><a href="#">
                <time datetime="2018-07-01">2018.07.01</time>
                <span>ホームページリニューアルしました！</span>
              </a></li>
          </ul>
          <div class="news_btn"><a href="">一覧へ</a></div>
        </div>
      </section><!-- end news -->
      <section id="company" class="company">
        <figure><img src="<?php bloginfo('template_url')?>/images/company.jpg" alt="company"></figure>
        <div class="company_pos">
          <div class="company_main row">
            <div class="company_main_l">
              <h2>COMPANY</h2>
              <span>会社概要</span>
              <p>カネイシ自動車は広島県呉市の自動車整備工場です。<br>車検・日常点検・整備・各種取付サービスなど、<br>お車のことならカネイシ自動車にお任せください。<br>確かな技術でお客様の安心、安全なカーライフを<br>オールサポートいたします。</p>
              <div class="_btn"><a href="/company">会社概要へ</a></div>
            </div>
          </div>
        </div>
      </section><!-- end company -->
      <section id="service" class="service">
        <div class="service_top">
          <div class="service_ttl">
            <h2>SERVICE</h2>
            <span>サービス内容</span>
          </div>
          <div class="service_c row">
            <div class="service_box">
              <a href="<?php _e(home_url())?>/inspection">
                <div class="service_box_t">
                  <figure><img src="<?php bloginfo('template_url')?>/images/service_01.jpg" alt="service 01"></figure>
                  <p><img src="<?php bloginfo('template_url')?>/images/txt_01.png" alt="車検サービス"></p>
                </div>
                <ul class="service_box_b">
                  <li>・車検サービス</li>
                  <li>・レッカー引取り代車</li>
                </ul>
              </a>
            </div>
            <div class="service_box">
              <a href="<?php _e(home_url())?>/purchase">
                <div class="service_box_t">
                  <figure><img src="<?php bloginfo('template_url')?>/images/service_02.jpg" alt="service 02"></figure>
                  <p><img src="<?php bloginfo('template_url')?>/images/txt_02.png" alt="購入買取"></p>
                </div>
                <ul class="service_box_b">
                  <li>・新車</li>
                  <li>・中古車</li>
                  <li>・愛車買取</li>
                  <li>・各種保険</li>
                </ul>
              </a>
            </div>
            <div class="service_box">
              <a href="<?php _e(home_url())?>/bankin-toso">
                <div class="service_box_t">
                  <figure><img src="<?php bloginfo('template_url')?>/images/service_03.jpg" alt="service 03"></figure>
                  <p><img src="<?php bloginfo('template_url')?>/images/txt_03.png" alt="鈑金・塗装"></p>
                </div>
                <ul class="service_box_b">
                  <li>・一般整備</li>
                  <li>・板金、塗装、ドレスアップ</li>
                  <li>・ボディーコーテング</li>
                </ul>
              </a>
            </div>
          </div>
          <div class="service_ex row">
            <p>ご来店前に！チェック！<br>カネイシ自動車の<span>中古車情報</span>はこちらから！</p>
            <ul>
              <li><a href="https://www.carsensor.net/shop/hiroshima/307423001/#contents" target="_blank"><img src="<?php bloginfo('template_url')?>/images/service_img_04.png" alt="service 04"></a></li>
              <li><a href="https://www.goo-net.com/usedcar_shop/1002093/detail.html" target="_blank"><img src="<?php bloginfo('template_url')?>/images/service_img_05.png" alt="service 05"></a></li>
            </ul>
          </div>
        </div>
        <div class="service_bottom">
          <ul>
            <li><img src="<?php bloginfo('template_url')?>/images/service_extra_01.jpg" alt="service extra 01"></li>
            <li><img src="<?php bloginfo('template_url')?>/images/service_extra_02.jpg" alt="service extra 02"></li>
            <li><img src="<?php bloginfo('template_url')?>/images/service_extra_03.jpg" alt="service extra 03"></li>
          </ul>
        </div>
      </section><!-- end service -->
      <div id="partner" class="partner row">
        <ul>
          <li><a href="#"><img src="<?php bloginfo('template_url')?>/images/partner_01.jpg" alt="partner"></a></li>
          <li><a href="#"><img src="<?php bloginfo('template_url')?>/images/partner_02.jpg" alt="partner"></a></li>
          <li><a href="#"><img src="<?php bloginfo('template_url')?>/images/partner_03.jpg" alt="partner"></a></li>
        </ul>
      </div><!-- end partner -->
      <div id="map" class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13195.410256424344!2d132.5996662593651!3d34.22678277776842!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3550091fdd175679%3A0xeb61b9446641a9c4!2s3+Chome-5-7+Hirotagaya%2C+Kure-shi%2C+Hiroshima-ken+737-0134%2C+Japan!5e0!3m2!1sen!2s!4v1541746166630" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div><!-- end map -->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/templates','footer')?>
    </footer>
  </div><!-- end container -->
  <?php get_footer();?>
</body>
</html>