<?php
/*
  Template Name: Inspect Page
 */
get_header();
?>
<body>

<div id="container">
  <header id="header" class="header">
    <?php get_template_part('templates/templates','header')?>
  </header>
  <main>
    <section id="inspect" class="inspect">
      <div class="ttl">
        <div class="row">
          <h2 class="ttl_inner">車検サービス</h2>
        </div>
      </div>
      <div class="inspect_inner">
        <div class="row">
          <div class="inspect_inner_list">
            <dl>
              <dt>
                <h3>
                  <img src="<?php bloginfo('template_url')?>/inspect/images/tt_inspect.png?v=65fc51cb483ec580ae5eccc74aaae1b1" alt="title">
                  <span>国の指定工場で自社検査</span>
                </h3>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/inspect/images/img_01.jpg?v=f2566a84bbdac394ee4dfc0f147478a7" alt="image">
                </figure>
              </dt>
              <dd>
                <p>弊社は陸運局認定指定整備工場のため、管轄の陸運局にお客様のお車を運ぶことなく自社にて一括して作業を完了させることができます。また、各種講習会や研修などにも積極的に参加し日々技術力の向上、接客スキルの向上にも前向きに取り組んでおります。<br>最新技術を搭載した新型車にも幅広く対応しており、故障診断器も完備していますので随時コンピューター診断も承っております。法人様の営業車も対応させていただいている中で、特にタクシーは、適切なメンテナンスをすることで50km、100kmと走行距離が延びます。幅広い知識と技術力、万が一の時のサポート力が、弊社の強みです。ハイブリッド車、電気自動車と先進技術が盛り込まれた車が販売される中で必要な知識を備え、正しい作業手順に沿ってお客様のお車に携わることが最大のサービスと考えております。</p>
                <p>整備後は施工前の状態、今の状態を説明し、必要とあれば取り替えた部品の照合なども行っていただけます。見えないところだからこそ、見える形を目指し、お客様の信用と安心に繋げていくサービスを常に考え、ご要望とあれば可能な限りお応えさせていただきます。</p>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/inspect/images/img_inspect.jpg?v=62854c95e206d493646fc3f08c5207f6" alt="">
                </figure>
                <a href=""><img src="<?php bloginfo('template_url')?>/inspect/images/banner.jpg?v=9a9b5f2942a5ea11cec886b709d6dd57" alt=""></a>
              </dd>
            </dl>
          </div>
        </div>
      </div>
    </section>
  </main>
  <footer id="footer" class="footer">
    <?php get_template_part('templates/templates','footer')?>
  </footer>
</div>
<?php get_footer();?>
</body>
</html>
