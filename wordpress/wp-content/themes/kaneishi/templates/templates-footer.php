<div class="bx_footer">
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <div class="top_ft">
    <div class="row">
      <ul class="nav_ft show_pc">
        <li><a href="<?php _e(home_url())?>">ホーム</a></li>
        <li><a href="<?php _e(home_url())?>/service">サービス内容</a></li>
        <li><a href="<?php _e(home_url())?>/company">会社概要</a></li>
        <li><a href="#">採用情報</a></li>
        <li><a href="https://www.facebook.com/kaneishijidousya/" target="_blank">Facebook</a></li>
      </ul>
      <!--/.nav_ft-->
      <div class="bx_aff">
        <div class="bx_aff_left">
          <h3 class="ttl_aff">関連企業</h3>
          <ul>
            <li>
              <figure>
                <a href="https://111.owst.jp/" target="_blank">
                  <img src="<?php bloginfo('template_url')?>/common/images/img_n_01.png" alt="img 01">
                </a>
              </figure>
              <em>
                ENTERTAINMENT SOUND BAR 111<br>
                バー・カクテル/ダーツバー・スポーツバー<br>
                公式サイトはこちらから
              </em>
            </li>
            <li>
              <figure>
                <a href="https://tabelog.com/hiroshima/A3404/A340401/34021523/" target="_blank">
                  <img src="<?php bloginfo('template_url')?>/common/images/img_n_02.png" alt="img 02">
                </a>
              </figure>
              <em>
                海のYEAH!!! <br>
                魚介料理・海鮮料理<br>
                公式サイト（ホットペッパー）はこちらから
              </em>
            </li>
          </ul>
        </div>
        <div class="bx_aff_right">
          <div class="facebook">
            <div class="fb-face">
              <iframe id="facebook_iframe" class="facebook_iframe"></iframe>
            </div>
          </div>
        </div>
      </div>
      <!--/.bx_aff-->
    </div>
  </div>
  <!--/.top_footer-->
  <div class="bottom_ft">
    <ul class="n_bt">
      <li><a href="#">プライバシポリシー</a></li>
      <li><a href="#">サイトマツプ</a></li>
    </ul>
    <!--/.n_bt-->
    <p class="copyright">
      &copy; Kaneishi. All Rights Reserved.
    </p>
  </div>
  <!--/.bottom_ft-->
  <div class="footer_pagetop">
    <div class="row">
      <a href="" class="gotop" id="gotop">
        <img src="<?php bloginfo('template_url')?>/common/images/pagetop.png" alt="Pagetop">
      </a>
    </div>
  </div>
</div>
<!--/.bx_footer-->