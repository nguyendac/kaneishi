<?php
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');
function enqueue_script(){
  wp_enqueue_script('libs',get_template_directory_uri().'/common/js/libs.js', '1.0', 1 );
  if(is_front_page()) {
    wp_enqueue_script('toppage', get_template_directory_uri().'/js/script.js', '1.0', 1 );
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_script');
function enqueue_style() {
  if(is_front_page()) {
    wp_enqueue_style('toppage',get_stylesheet_directory_uri().'/css/style.css');
  }
  if(is_page_template('company/company.php')) {
    wp_enqueue_style( 'company-style', get_stylesheet_directory_uri() . '/company/css/style.css');
  }
  if(is_page_template('inspect/inspect.php')) {
    wp_enqueue_style( 'inspect-style', get_stylesheet_directory_uri() . '/inspect/css/style.css');
  }
  if(is_page_template('service/service.php') || is_page_template('service/banking_toso.php') || is_page_template('service/inspection.php') || is_page_template('service/purchase.php')) {
    wp_enqueue_style( 'service-style', get_stylesheet_directory_uri() . '/service/css/style.css');
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_style' );
?>