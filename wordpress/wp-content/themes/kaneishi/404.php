<?php
get_header();
?>
<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/templates','header')?>
    </header>
    <main>
      <section class="company">
        <div class="ttl">
          <div class="row">
            <h2 class="ttl_inner">Page Not Found</h2>
          </div>
        </div>
        <!--/.gr_company-->
      </section>
      <!--/.company-->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/templates','footer')?>
    </footer>
  </div><!-- end container -->
  <?php get_footer();?>
</body>
</html>