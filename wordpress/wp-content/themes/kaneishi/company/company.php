<?php
/*
  Template Name: Company Page
 */
get_header();
?>
<body>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('templates/templates','header')?>
    </header>
    <main>
      <section class="company">
        <div class="ttl">
          <div class="row">
            <h2 class="ttl_inner">会社概要</h2>
          </div>
        </div>
        <div class="gr_company">
          <div class="row">
            <ul>
              <li>
                <span>会社名</span>
                <em>有限会社 カネイシ自動車 &nbsp;&nbsp;(英語表記)&nbsp;&nbsp;KANEISHI AUOT inc.</em>
              </li>
              <li>
                <span>代表者</span>
                <em>国久保　純</em>
              </li>
              <li>
                <span>設立</span>
                <em>昭和60年5月10日</em>
              </li>
              <li>
                <span>住所</span>
                <em>〒737-0134  広島県呉市広多賀谷３丁目５−７</em>
              </li>
              <li>
                <span>業務内容</span>
                <ul class="sub_li">
                  <li>
                    自動車設備<br>
                    自動車及び各種板金
                  </li>
                  <li>
                    各種部品塗装<br>
                    損害保険・自動車保険の代理店
                  </li>
                  <li>
                    製麺保険<br>
                    レンタカー
                  </li>
                </ul>
              </li>
              <li>
                <span>従業員</span>
                <em>10名(バイト含む)</em>
              </li>
              <li>
                <span>営業時間</span>
                <em>月曜日〜土曜日(8:00~18:00)</em>
              </li>
              <li>
                <span>グループ会社</span>
                <em>株式会社111海のYeah!!</em>
              </li>
            </ul>
          </div>
        </div>
        <!--/.gr_company-->
      </section>
      <!--/.company-->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('templates/templates','footer')?>
    </footer>
  </div><!-- end container -->
  <?php get_footer();?>
</body>
</html>