window.addEventListener('DOMContentLoaded',function(){
  new Slide();
})
var Slide = (function(){
  function Slide(){
    var s = this;
    this.target = 'under';
    this.max_height_target;
    this.step  = 0.00035;
    this.scale = 1;
    this.currentSlide = 0;
    this.opacity = 0;
    this.timer;
    this.timeout;
    this.img;
    this.eles = document.getElementById(this.target).querySelectorAll('ul li img');
    this.func_transition = function(){
      s.img = s.eles[s.currentSlide];
      s.img.closest('li').style.opacity = 1;
      s.scale+=s.step;
      s.img.style[transformProperty] = 'scale('+s.scale+')';
      if(s.scale >= 1.15){
        s.currentSlide+=1;
        s.img.closest('li').style.opacity = 0;
        s.timeout = setTimeout(function(){
          s.img.style[transformProperty] = 'scale(1)';
        },2000);
        s.scale = 1;
        if(s.currentSlide > s.eles.length-1) {
          s.currentSlide = 0;
        }
      }
      s.timer = window.requestAnimFrame(s.func_transition);
    }
    this.sizeWindow = function(){
      Array.prototype.forEach.call(s.eles,function(el,i){
        el.closest('li').style.opacity = s.opacity;
        var img = new Image();
        img.onload = function(){
          s.getRatio(img);
        }
        img.src = el.getAttribute('src');
      });
    }
    this.getRatio = function(img){
      var ratio;
      ratio = img.naturalWidth/img.naturalHeight;
      return ratio;
    }
    window.addEventListener('resize',function(){
      s.sizeWindow();
    })
    window.addEventListener('load',function(){
      s.sizeWindow();
    })
    this.sizeWindow();
    this.func_transition();
  }
  return Slide;
})()