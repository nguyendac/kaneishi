<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Description" content="<?php bloginfo('description'); ?>">
<meta name="Keywords" content="<?php bloginfo('keywords'); ?>">
<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
<meta property="og:title" content="<?php bloginfo('name')?>">
<meta property="og:url" content="">
<meta property="og:image" content="">
<meta property="og:type" content="website">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url')?>/common/css/normalize.css">
<link rel="stylesheet" href="<?php bloginfo('template_url')?>/common/css/common.css">
<?php wp_head();?>
</head>