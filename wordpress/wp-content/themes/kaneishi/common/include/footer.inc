<div class="bx_footer">
  <div class="top_ft">
    <div class="row">
      <ul class="nav_ft show_pc">
        <li><a href="/">ホーム</a></li>
        <li><a href="/service">サービス内容</a></li>
        <li><a href="/company">会社概要</a></li>
        <li><a href="#">採用情報</a></li>
        <li><a href="#">Facebook</a></li>
      </ul>
      <!--/.nav_ft-->
      <div class="bx_aff">
        <div class="bx_aff_left">
          <h3 class="ttl_aff">関連企業</h3>
          <ul>
            <li>
              <figure>
                <img src="/common/images/img_n_01.png" alt="img 01">
              </figure>
              <em>
                ENTERTAINMENT SOUND BAR 111<br>
                バー・カクテル/ダーツバー・スポーツバー<br>
                公式サイトはこちらから
              </em>
            </li>
            <li>
              <figure>
                <img src="/common/images/img_n_02.png" alt="img 02">
              </figure>
              <em>
                海のYEAH!!! <br>
                魚介料理・海鮮料理<br>
                公式サイト（ホットペッパー）はこちらから
              </em>
            </li>
          </ul>
        </div>
        <div class="bx_aff_right">
          <div class="facebook">
            <div class="fb-face">
              <iframe id="facebook_iframe" class="facebook_iframe"></iframe>
            </div>
          </div>
        </div>
      </div>
      <!--/.bx_aff-->
    </div>
  </div>
  <!--/.top_footer-->
  <div class="bottom_ft">
    <ul class="n_bt">
      <li><a href="#">プライバシポリシー</a></li>
      <li><a href="#">サイトマツプ</a></li>
    </ul>
    <!--/.n_bt-->
    <p class="copyright">
      &copy; Kaneishi. All Rights Reserved.
    </p>
  </div>
  <!--/.bottom_ft-->
  <div class="footer_pagetop">
    <div class="row">
      <a href="" class="gotop" id="gotop">
        <img src="/common/images/pagetop.png" alt="Pagetop">
      </a>
    </div>
  </div>
</div>
<!--/.bx_footer-->