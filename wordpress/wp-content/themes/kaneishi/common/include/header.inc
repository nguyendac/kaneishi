<div class="bx_header">
  <div class="bx_header_top">
    <div class="row wrap">
      <div class="hd_left">
        <h1 class="logo"><a href=""><img src="/common/images/logo.png" alt="Logo"></a></h1>
      </div>
      <!--/.hd_left-->
      <div class="hd_right">
        <div class="hd_right_ctn">
          <p><span>TEL.<a class="tel" href="tel:0823-71-3901">0823-71-3901</a></span><span>FAX.0823-74-1642</span></p>
          <em class="show_pc">[営業時間]　平日 8:30-19:30 / 土曜日 17:30まで</em>
        </div>
        <!--/.contact-->
        <div class="hd_right_btn show_pc" id="btn_contact">
          <a class="btn_ct" href="#"><span>お問い合わせ</span></a>
        </div>
        <!--/.btn_contact-->
        <div class="menu show_sp" id="icon_nav">
          <div class="icon_menu">
            <div class="icon_inner"></div>
          </div>
        </div>
        <!--/.icon_nav-->
      </div>
      <!--/.hd_right-->
    </div>
  </div>
  <!--/.top-->
  <div class="bx_header_bottom">
    <div class="row">
      <nav class="nav" id="nav">
        <ul>
          <li><a href="/"><span>ホーム</span></a></li>
          <li><a href="/service"><span>サービス内容</span></a></li>
          <li><a href="/company"><span>会社概要</span></a></li>
          <li><a href="#"><span>採用情報</span></a></li>
        </ul>
      </nav>
    </div>
  </div>
  <!--/.bottom-->
</div>
<!--/.bx_header-->