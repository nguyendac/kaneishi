window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(callback){
    return window.setTimeout(callback, 1000/60);
  };
})();

window.cancelAnimFrame = (function(_id) {
  return window.cancelAnimationFrame ||
  window.cancelRequestAnimationFrame ||
  window.webkitCancelAnimationFrame ||
  window.webkitCancelRequestAnimationFrame ||
  window.mozCancelAnimationFrame ||
  window.mozCancelRequestAnimationFrame ||
  window.msCancelAnimationFrame ||
  window.msCancelRequestAnimationFrame ||
  window.oCancelAnimationFrame ||
  window.oCancelRequestAnimationFrame ||
  function(_id) { window.clearTimeout(id); };
})();
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
function closest(el, selector) {
  // type el -> Object
  // type select -> String
  var matchesFn;
  // find vendor prefix
  ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}

function getCssProperty(elem, property) {
  return window.getComputedStyle(elem, null).getPropertyValue(property);
}
var transform = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"];
var flex = ['-webkit-box', '-moz-box', '-ms-flexbox', '-webkit-flex', 'flex'];
var fd = ['flexDirection', '-webkit-flexDirection', '-moz-flexDirection'];
var animatriondelay = ["animationDelay","-moz-animationDelay","-wekit-animationDelay"];
function getSupportedPropertyName(properties) {
  for (var i = 0; i < properties.length; i++) {
    if (typeof document.body.style[properties[i]] != "undefined") {
      return properties[i];
    }
  }
  return null;
}
var transformProperty = getSupportedPropertyName(transform);
var flexProperty = getSupportedPropertyName(flex);
var fdProperty = getSupportedPropertyName(fd);
var ad = getSupportedPropertyName(animatriondelay);
var easingEquations = {
  easeOutSine: function(pos) {
    return Math.sin(pos * (Math.PI / 2));
  },
  easeInOutSine: function(pos) {
    return (-0.5 * (Math.cos(Math.PI * pos) - 1));
  },
  easeInOutQuint: function(pos) {
    if ((pos /= 0.5) < 1) {
      return 0.5 * Math.pow(pos, 5);
    }
    return 0.5 * (Math.pow((pos - 2), 5) + 2);
  }
};

function isPartiallyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}

function isFullyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  return ((top >= 0) && (bottom <= window.innerHeight));
}

function CreateElementWithClass(elementName, className) {
  var el = document.createElement(elementName);
  el.className = className;
  return el;
}

function createElementWithId(elementName, idName) {
  var el = document.createElement(elementName);
  el.id = idName;
  return el;
}

function getScrollbarWidth() {
  var outer = document.createElement("div");
  outer.style.visibility = "hidden";
  outer.style.width = "100px";
  document.body.appendChild(outer);
  var widthNoScroll = outer.offsetWidth;
  // force scrollbars
  outer.style.overflow = "scroll";
  // add innerdiv
  var inner = document.createElement("div");
  inner.style.width = "100%";
  outer.appendChild(inner);
  var widthWithScroll = inner.offsetWidth;
  // remove divs
  outer.parentNode.removeChild(outer);
  return widthNoScroll - widthWithScroll;
}

function insertAfter(referenceNode, newNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
var wordsToArray = function wordsToArray(str) {return str.split('').map(function (e) {return e === ' ' ? '&nbsp;' : e;});};

function insertSpan(elem, letters, startTime) {
  elem.textContent = ''
  var curr = 0
  var delay = 20
  letters.forEach(function(letter,i){
    var span = document.createElement('span');
    span.classList.add('waveText');
    span.innerHTML = letter
    span.style[ad] = (++curr / delay + (startTime || 0)) + 's'
    elem.appendChild(span)
  })
}
function getPosition(el) {
  var xPos = 0;
  var yPos = 0;
  while (el) {
    if (el.tagName == "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      var yScroll = el.scrollTop || document.documentElement.scrollTop;

      xPos += (el.offsetLeft - xScroll + el.clientLeft);
      yPos += (el.offsetTop - yScroll + el.clientTop);
    } else {
      // for all other non-BODY elements
      xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      yPos += (el.offsetTop - el.scrollTop + el.clientTop);
    }
    el = el.offsetParent;
  }
  return {
    x: xPos,
    y: yPos
  };
}
/* images pc <---> sp */
(function () {
  var PicturePolyfill = (function () {
    function PicturePolyfill() {
      var _this = this;
      this.pictures = [];
      this.onResize = function () {
        var width = document.body.clientWidth;
        for (var i = 0; i < _this.pictures.length; i = (i + 1)) {
          _this.pictures[i].update(width);
        };
      };
      if ([].includes) return;
      var picture = Array.prototype.slice.call(document.getElementsByTagName('picture'));
      for (var i = 0; i < picture.length; i = (i + 1)) {
        this.pictures.push(new Picture(picture[i]));
      };
      window.addEventListener("resize", this.onResize, false);
      this.onResize();
    }
    return PicturePolyfill;
  }());
  var Picture = (function () {
    function Picture(node) {
      var _this = this;
      this.update = function (width) {
        width <= _this.breakPoint ? _this.toSP() : _this.toPC();
      };
      this.toSP = function () {
        if (_this.isSP) return;
        _this.isSP = true;
        _this.changeSrc();
      };
      this.toPC = function () {
        if (!_this.isSP) return;
        _this.isSP = false;
        _this.changeSrc();
      };
      this.changeSrc = function () {
        var toSrc = _this.isSP ? _this.srcSP : _this.srcPC;
        _this.img.setAttribute('src', toSrc);
      };
      this.img = node.getElementsByTagName('img')[0];
      this.srcPC = this.img.getAttribute('src');
      var source = node.getElementsByTagName('source')[0];
      this.srcSP = source.getAttribute('srcset');
      this.breakPoint = Number(source.getAttribute('media').match(/(\d+)px/)[1]);
      this.isSP = !document.body.clientWidth <= this.breakPoint;
      this.update();
    }
    return Picture;
  }());
  new PicturePolyfill();
}());


window.addEventListener('DOMContentLoaded', function() {
  new ExtraNav();
  new Menu();
  new Sticky();
  new Resface();
  new PageTop();
});

var ExtraNav = function () {
  function ExtraNav() {
    var e = this;
    this.clone = document.getElementById('btn_contact').cloneNode(true);
    this.clone.removeAttribute('id');
    this.clone.classList.remove('show_pc');
    this.clone.classList.add('show_sp');
    this.des = document.getElementById('nav');
    this.des.appendChild(this.clone);
  }
  return ExtraNav;
}();

var Menu = (function(){
  function Menu(){
    var m = this;
    this._target = document.getElementById('icon_nav');
    this._mobile = document.getElementById('nav');
    this._header = document.getElementById('header');
    this._target.addEventListener('click',function(){
      if(this.classList.contains('open')){
        this.classList.remove('open');
        m._mobile.classList.remove('open');
        m._mobile.style.height = 0;
        document.body.style.overflow = 'inherit';
      } else {
        this.classList.add('open');
        m._mobile.classList.add('open');
        document.body.style.overflow = 'hidden';
        m._mobile.style.height = window.innerHeight-closest(m._target,'header').clientHeight+'px';
        m._mobile.style.top = m._header.clientHeight +'px';
      }
    })
    this._reset = function(){
      if(m._target.classList.contains('open')){
        if(window.innerWidth > 768) {
          m._target.classList.remove('open');
          m._mobile.classList.remove('open');
          document.body.style.overflow = 'auto';
          m._mobile.style.height = 'auto';
          document.body.style.paddingTop = m._header.clientHeight+'px';
          m._mobile.style.height = 'auto';
        } else {
          m._mobile.style.height = window.innerHeight-closest(m._target,'header').clientHeight+'px';
          m._mobile.style.top = m._header.clientHeight +'px';
        }
      } else {
        if(window.innerWidth < 769) {
          m._mobile.style.height = 0;
        } else {
          m._mobile.style.height = 'auto';
        }
      }
    }
    this._reset();
    window.addEventListener('resize',m._reset,false);
  }
  return Menu;
})();
var Sticky = function () {
  function Sticky() {
    var s = this;
    this._target = document.getElementById('header');
    this._basic_height = this._target.clientHeight;
    this._mobile = document.getElementById('nav');
    this.bx_bottom = document.querySelector('.bx_header_bottom');
    this._for_sp = function(top){
      s._target.style.left = 0;
      document.body.style.paddingTop = s._target.clientHeight+'px';
      if(top > 0) {
        s._target.classList.add('fixed');
        document.body.style.paddingTop = s._target.clientHeight+'px';
      } else {
        s._target.classList.remove('fixed');
        document.body.style.paddingTop = 0;
      }
    }
    this._for_pc = function (top, _left) {
      s._mobile.style.top = 0;
      var offset = s._target.querySelector('.bx_header_top').clientHeight;
      var bx_bottom = s._target.querySelector('.bx_header_bottom').clientHeight;
      if (top > offset) {
        s._target.classList.add('fixed');
        s._mobile.style.left = -_left + 'px';
        document.body.style.paddingTop = s.bx_bottom.clientHeight + parseInt(getCssProperty(s.bx_bottom, 'border-top-width')) + 'px';
      } else {
        document.body.style.paddingTop = 0;
        s._target.classList.remove('fixed');
        s._mobile.style.left = 0;
      }
    };
    this.handling = function () {
      var _top = document.documentElement.scrollTop || document.body.scrollTop;
      var _left = document.documentElement.scrollLeft || document.body.scrollLeft;
      if (window.innerWidth < 769) {
        s._for_sp(_top);
      } else {
        s._for_pc(_top, _left);
      }
    };
    window.addEventListener('scroll', s.handling, false);
    window.addEventListener('resize', s.handling, false);
    window.addEventListener('load', s.handling, false);
  }
  return Sticky;
}();

var Resface = (function(){
  function Resface(){
    var r = this;
    this.resizeTimeout;
    this._target = document.getElementById('facebook_iframe');
    this.screen = window.innerWidth;
    this.actualResizeHandler = function(){
      var width,height,src;
      if(window.innerWidth > 500) {
        width = height = 500;
      } else {
        width = height = window.innerWidth;
      }
      src = "https://www.facebook.com/v3.0/plugins/page.php"+
      "?adapt_container_width=true&app_id=812696812175003"+
      "&height="+ width+
      "&width="+ height+
      "&hide_cover=false"+
      "&href=https%3A%2F%2Fwww.facebook.com%2Ffacebook%2F&locale=ja_JP&sdk=joey&show_facepile=false&small_header=false&tabs=timeline";
      r._target.src = src;
      r._target.width = width;
      r._target.height = height;
    }
    this.resizeThrottler = function(){
      if(!r.resizeTimeout && r.screen != window.innerWidth) {
        r.resizeTimeout = setTimeout(function() {
          r.resizeTimeout = null;
          r.actualResizeHandler();
        }, 66);
      }
    }
    window.addEventListener("load", r.actualResizeHandler, false);
    window.addEventListener("resize", r.resizeThrottler, false);
  }
  return Resface;
})()
var PageTop = (function(){
  function PageTop(){
    var pa = this;
    this._target = document.getElementById('gotop');
    this.flag_start = false;
    this.stopEverything = function(){
      pa.flag_start = false;
    }
    this.scrollToY = function(scrollTargetY,speed,easing){
      var scrollY = window.scrollY || window.pageYOffset,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;
      var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));
      function tick() {
        if(pa.flag_start){
          currentTime += 1 / 60;
          var p = currentTime / time;
          var t = easingEquations[easing](p);
          if (p < 1) {
            requestAnimFrame(tick);
            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
          } else {
            window.scrollTo(0, scrollTargetY);
          }
        }
      }
      tick();
    }
    this._target.addEventListener('click',function(e){
      e.preventDefault();
      pa.flag_start = true;
      pa.scrollToY(0,1000,'easeOutSine');
    })
    document.querySelector("body").addEventListener('mousewheel',pa.stopEverything,false);
    document.querySelector("body").addEventListener('DOMMouseScroll',pa.stopEverything,false);
  }
  return PageTop;
})();